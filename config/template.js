var config = {};

//-----------------------------------------------

config.site_title      = "Mythos";
config.title_separator = "|";

//-----------------------------------------------

module.exports = config;