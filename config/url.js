var config = {};

//-----------------------------------------------

config.base_url   = "http://localhost:3000/";
config.assets_url = config.base_url + "assets/";
config.bower_url  = config.assets_url + "bower/";

//-----------------------------------------------

module.exports = config;