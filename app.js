var Express  = require('express');
var Debug    = require('debug');
var Mythos = require('mythos-core-mythos');

var app      = new Express();
var debug    = new Debug('mythos');

Mythos.init(app);

var server = app.listen(app.get('port'), function() {
	debug('Express server listening on port ' + server.address().port);
});
