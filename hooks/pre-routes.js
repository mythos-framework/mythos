var bodyParser   = require('body-parser');
var cookieParser = require('cookie-parser');
var logger       = require('morgan');

module.exports = function (app) {
	"use strict";

	app.use(function (req, res, next) {
		var mythos = res.locals.mythos;
		mythos.lib.template.bindMiddleware(req, res);
		next();
	});
	app.use(logger('dev'));
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded());
	app.use(cookieParser());
};