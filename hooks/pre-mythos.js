var Express = require('express');
var favicon = require('static-favicon');
var nib     = require('nib');
var path    = require('path');
var stylus  = require('stylus');

module.exports = function (app) {
	"use strict";

	app.disable('x-powered-by');
	app.set('port', process.env.PORT || 3000);
	app.set('views', path.join(__dirname, '..', 'views'));
	app.set('view engine', 'jade');

	app.use(stylus.middleware({
		"src"      : path.join(__dirname, '..', 'css'),
		"dest"     : path.join(__dirname, '..', 'public', 'assets', 'css'),
		"compile"  : function (str, path) {
			return stylus(str)
				.set('filename', path)
				.set('compress', true)
				.use(nib());
		}
  	}));

	// Static paths should be here to prevent mythos instantiating every HTTP request of a static file.
	app.use(favicon());
	app.use(Express.static(path.join(__dirname, '..', 'public')));
};