var ExpressPath  = require('express-path');
var path         = require('path');

module.exports = function (app) {
	"use strict";

	new ExpressPath(app, 'config/routes', {
		"expressRoot": path.join(__dirname, '..') + '/'
	});
};